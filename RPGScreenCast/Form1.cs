﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace RPGScreenCast
{

    public partial class Form1 : Form
    {

        static Image currentImage;
        private Image originalImage;
        static Thumbnail currentThumbnail;
        public static int currentPdfPage;
        bool isPointing;
        static string mainDirPath;
        static List<string> allImagesPaths = new List<string>();
        public static List<String> favsToDelete = new List<String>();
        public static List<String> originalFavs = new List<String>();

        static string[] fileTypes = new string[] { "jpg", "jpeg", "png", "bmp", "pdf" };
        public Form1()
        {
            InitializeComponent();
            this.DoubleBuffered = true;
            UpdatePreview(global::RPGScreenCast.Properties.Resources.otter, null);
            LoadCustomColors(); // Load custom colors on startup

            Application.ApplicationExit += CleanFiles;
        }

        private void CleanFiles(object sender, EventArgs e)
        {
            foreach (String path in favsToDelete)
            {
                File.Delete(path);
            }
        }

        public void UpdatePreview(Image img, Thumbnail thumbnail)
        {
            originalImage = (Image)img.Clone();
            currentImage = img;
            currentThumbnail = thumbnail;
            pictureBox_preview.Image = img;
            pictureBox_preview.Update();
            Program.previewerInstance.UpdateImage(pictureBox_preview.Image);

            ResetDrawing();
        }

        void UpdateFolder(string path)
        {
            flowLayoutPanel_dirContent.Controls.Clear();

            panel_loadbar.Visible = true;

            string[] allFiles = GetFilesFrom(path, fileTypes, false);
            for (int i = 0; i < allFiles.Length; i++)
            {
                var file = allFiles[i];
                bool match = false;
                foreach (string fileType in fileTypes)
                {
                    if (file.Contains(fileType))
                        match = true;
                }
                if (!match) continue;

                try
                {
                    Console.WriteLine(file);
                    Thumbnail thmb = new Thumbnail(file, false, false);
                    flowLayoutPanel_dirContent.Controls.Add(thmb);
                }
                catch (Exception e) { }

                progressBar_dirContent.Value = (int)((float)i / (float)allFiles.Length * 100.0f);
                progressBar_dirContent.Update();
            }
            flowLayoutPanel_dirContent.Update();
            panel_loadbar.Visible = false;
        }

        public static String[] GetFilesFrom(String searchFolder, String[] filters, bool isRecursive)
        {
            List<String> filesFound = new List<String>();
            var searchOption = isRecursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly;
            foreach (var filter in filters)
            {
                filesFound.AddRange(Directory.GetFiles(searchFolder, String.Format("*.{0}", filter), searchOption));
            }
            return filesFound.ToArray();
        }

        private void OnClickedFolderSelection(object sender, MouseEventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.RestoreDirectory = true;
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                //UpdateFolder(dialog.FileName);.
                LoadDirectory(dialog.FileName);
                label_dirPath.Text = dialog.FileName;
            }
        }

        private void OnClickedOpenPreviewer(object sender, MouseEventArgs e)
        {
            Program.previewerInstance = new FullScreenWindow();
            Program.previewerInstance.ShowDialog();
            Program.previewerInstance.UpdateImage(currentImage);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Program.previewerInstance.Close();


        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void OnForceFullscreen(object sender, EventArgs e)
        {
            Program.previewerInstance.ClickFullscreen(new object(), new EventArgs());
        }

        private PointF NormalizePreviewImagePos(PointF PreviewImagePos)
        {
            float imageAspect = (float)pictureBox_preview.Image.Width / pictureBox_preview.Image.Height;
            float boxAspect = (float)pictureBox_preview.Width / pictureBox_preview.Height;
            PointF imageSize;
            if (imageAspect < boxAspect)
                imageSize = new PointF(imageAspect * pictureBox_preview.Height, pictureBox_preview.Height);
            else if (imageAspect > boxAspect)
                imageSize = new PointF(pictureBox_preview.Width, pictureBox_preview.Width / imageAspect);
            else
                imageSize = new PointF(pictureBox_preview.Width, pictureBox_preview.Height);

            PointF borderSize = new PointF((pictureBox_preview.Width - imageSize.X) / 2f, (pictureBox_preview.Height - imageSize.Y) / 2f);
            PointF normalizedImagePos = new PointF((PreviewImagePos.X - borderSize.X) / imageSize.X, (PreviewImagePos.Y - borderSize.Y) / imageSize.Y);

            //imprecision factor : normalizedPos is 0.98 when should be 1, factor is 1.02
            float imprecisionFactor = 1.01f;
            normalizedImagePos = new PointF(normalizedImagePos.X * imprecisionFactor, normalizedImagePos.Y * imprecisionFactor);

            return normalizedImagePos;
        }

        private void OnPointing(object sender, MouseEventArgs e)
        {
            if (!isPointing) return;

            Program.previewerInstance.PointOnImage(NormalizePreviewImagePos(new PointF(e.Location.X, e.Location.Y)));

            //button3.Location = new Point((int)(normalizedToImagePos.X * imageSize.X + borderSize.X), (int)(normalizedToImagePos.Y * imageSize.Y + borderSize.Y));

        }

        private void ClickPointingButton(object sender, EventArgs e)
        {
            isPointing = !isPointing;
            button_point.Text = isPointing ? "Arrêter" : "Pointer";
            if (isPointing)
                OnPointingStart(new object(), new EventArgs());
            else
                OnPointingEnd(new object(), new EventArgs());
        }

        private void OnPointingEnd(object v, EventArgs eventArgs)
        {
            isPointing = false;
            Program.previewerInstance.UpdatePointerVisiblity(false);
        }

        private void OnPointingStart(object v, EventArgs eventArgs)
        {
            isPointing = true;
            Program.previewerInstance.UpdatePointerVisiblity(true);
        }

        private void LoadDirectory(string Dir)
        {
            treeView_dirs.Nodes.Clear();
            allImagesPaths = new List<string>();
            mainDirPath = Dir;
            DirectoryInfo di = new DirectoryInfo(Dir);
            TreeNode tds = treeView_dirs.Nodes.Add(di.Name);
            tds.Tag = di.FullName;
            tds.StateImageIndex = 0;
            LoadSubDirectories(Dir, tds);
            treeView_dirs.TopNode.Expand();
            //treeView1.ExpandAll();

            Console.WriteLine("All Images paths:");
            foreach (var file in allImagesPaths)
            {
                Console.WriteLine(file);
            }

        }

        private void LoadSubDirectories(string dir, TreeNode td)
        {
            // Get all subdirectories  
            try
            {

                // Loop through all the images in the directory and add them to allImagesPaths
                foreach (var file in GetFilesFrom(dir, fileTypes, false))
                {
                    bool match = false;
                    foreach (string fileType in fileTypes)
                    {
                        if (file.Contains(fileType))
                            match = true;
                    }
                    if (!match) continue;

                    try
                    {
                        allImagesPaths.Add(file);
                    }
                    catch (Exception e) { }
                }

                // Loop through them to see if they have any other subdirectories  
                string[] subdirectoryEntries = Directory.GetDirectories(dir);
                foreach (string subdirectory in subdirectoryEntries)
                {
                    DirectoryInfo di = new DirectoryInfo(subdirectory);
                    TreeNode tds = td.Nodes.Add(di.Name);
                    tds.StateImageIndex = 0;
                    tds.Tag = di.FullName;
                    LoadSubDirectories(subdirectory, tds);
                }
            }
            catch (Exception e) { }

        }

        private void OnClickedFolderNode(object sender, TreeNodeMouseClickEventArgs e)
        {
            UpdateFolder(e.Node.Tag.ToString());
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void OnFavDragEnter(object sender, DragEventArgs e)
        {
            Thumbnail data = (Thumbnail)e.Data.GetData(typeof(Thumbnail));
            e.Effect = data.isFavorite ? DragDropEffects.None : DragDropEffects.Move;

        }

        private void OnFavDragDrop(object sender, DragEventArgs e)
        {
            Thumbnail data = (Thumbnail)e.Data.GetData(typeof(Thumbnail));

            originalFavs.Add(data.FilePath);

            Thumbnail thmb = new Thumbnail(data.FilePath, true, false);
            flowLayoutPanel_favsContent.Controls.Add(thmb);
            flowLayoutPanel_favsContent.Update();
            //thmb.SaveToFavFolder();
        }

        private void OnMainDragEnter(object sender, DragEventArgs e)
        {
            Thumbnail data = (Thumbnail)e.Data.GetData(typeof(Thumbnail));
            e.Effect = data.isFavorite ? DragDropEffects.Move : DragDropEffects.None;
        }

        private void OnMainDragDrop(object sender, DragEventArgs e)
        {
            Thumbnail data = (Thumbnail)e.Data.GetData(typeof(Thumbnail));
            flowLayoutPanel_favsContent.Controls.Remove(data);
            flowLayoutPanel_favsContent.Update();
        }

        private void RemoveFav(Thumbnail data)
        {
            originalFavs.Remove(data.FilePath);
            favsToDelete.Add(data.FavPath);
        }

        private void OnTrashButtonClick(object sender, MouseEventArgs e)
        {
            flowLayoutPanel_favsContent.Controls.Clear();
            flowLayoutPanel_favsContent.Update();
        }

        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        private void OnThmbRemovedFromFavs(object sender, ControlEventArgs e)
        {
            RemoveFav((Thumbnail)e.Control);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void SaveFavButton(object sender, EventArgs e)
        {
            string content = "";
            foreach (string file in originalFavs)
            {
                content += file + ";";
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "RPG Fav Files (*.rpgfav)|*.rpgfav";
            saveFileDialog.Title = "Save as RPG Favorite Files";

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, content);
            }
        }

        private void LoadFabButton(object sender, EventArgs e)
        {
            string content = "";
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "RPG Fav Files (*.rpgfav)|*.rpgfav";
            openFileDialog.Title = "Open RPG Favorite File";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                content = File.ReadAllText(openFileDialog.FileName);
                string[] files;

                //Split content by ";"
                files = content.Split(';');


                flowLayoutPanel_favsContent.Controls.Clear();
                foreach (string file in files)
                {
                    if (file == "" || !File.Exists(file)) continue;

                    Thumbnail thmb = new Thumbnail(file, true, true);
                    flowLayoutPanel_favsContent.Controls.Add(thmb);
                }
            }

        }

        private void ClickSearch(object sender, EventArgs e)
        {
            List<string> matches = new List<string>();

            if (searchBox.Text.Length != 0)
            {
                foreach (var file in allImagesPaths)
                {
                    //Only get the text after the last \
                    string[] parts = file.Split('\\');
                    if (parts[parts.Length - 1].IndexOf(searchBox.Text, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        matches.Add(file);
                        Console.WriteLine("Add image : " + file);
                    }
                }

                searchResult.Text = matches.Count + " résultat" + (matches.Count > 1 ? "s" : "");
                if (matches.Count > 0)
                    searchResult.ForeColor = Color.LightGreen;
                else
                    searchResult.ForeColor = Color.Coral;
                searchResult.Visible = true;
                searchResult.Update();
            }
            else
            {
                searchResult.Visible = false;
            }

            flowLayoutPanel_searchResult.Visible = matches.Count > 0;
            treeView_dirs.Visible = !(matches.Count > 0);

            if (matches.Count > 0)
            {
                flowLayoutPanel_searchResult.Controls.Clear();

                progressBar_search.Visible = true;
                int i = 0;
                foreach (var file in matches)
                {
                    bool match = false;
                    foreach (string fileType in fileTypes)
                    {
                        if (file.Contains(fileType))
                            match = true;
                    }
                    if (!match) continue;

                    try
                    {
                        Console.WriteLine(file);
                        Thumbnail thmb = new Thumbnail(file, false, false);
                        flowLayoutPanel_searchResult.Controls.Add(thmb);

                    }
                    catch (Exception ex) { }

                    i++;
                    progressBar_search.Value = (int)(((float)i / (float)matches.Count) * 100);
                    progressBar_search.Update();

                }
                progressBar_search.Visible = false;
                flowLayoutPanel_searchResult.Update();
            }
        }

        private void ClickClearSearch(object sender, EventArgs e)
        {
            flowLayoutPanel_searchResult.Controls.Clear();
            searchBox.Clear();

            flowLayoutPanel_searchResult.Visible = false;
            searchResult.Visible = false;
            treeView_dirs.Visible = true;

        }

        private void KeyPressSearch(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ClickSearch(sender, e);
            }
        }

        private void ClickPreviousPdfPage(object sender, EventArgs e)
        {
            SetCurrentPdfPage(-1);
        }

        private void ClickNextPdfPage(object sender, EventArgs e)
        {
            SetCurrentPdfPage(+1);
        }

        public void SetCurrentPdfPage(int increment)
        {
            if (currentThumbnail != null)
            {
                int pageCount = currentThumbnail.PdfPages.Count;
                if (pageCount > 1)
                {
                    flowpanel_pdfPages.Visible = true;
                    currentPdfPage = (currentPdfPage + increment) % pageCount;

                    button_prevPage.Enabled = !(currentPdfPage == 0);
                    button_nextPage.Enabled = !(currentPdfPage == pageCount - 1);

                    UpdatePreview(currentThumbnail.PdfPages[currentPdfPage], currentThumbnail);
                    label_pdfPage.Text = currentPdfPage + 1 + "/" + pageCount;
                }
                else
                    flowpanel_pdfPages.Visible = false;
            }
        }

        private Bitmap drawingBitmap;
        private Graphics drawingGraphics;
        private bool isDrawing = false;
        private bool eraseMode = false;
        private Point lastDrawingPos;
        private Point lastControlPos;
        private Color selectedColor = Color.Black;
        private Stack<Bitmap> undoStack = new Stack<Bitmap>();
        private Stack<Bitmap> redoStack = new Stack<Bitmap>();
        private int undoLimit = 10; // Default limit for undo stack

        private void ResetDrawing()
        {
            drawingBitmap = new Bitmap((Bitmap)pictureBox_preview.Image);
            drawingGraphics = Graphics.FromImage(drawingBitmap);

            // Clear undo/redo stacks
            undoStack.Clear();
            redoStack.Clear();
            UpdateUndoRedoButtonEnabled();

            // Subscribe to events
            pictureBox_preview.MouseDown += StartDrawing;
            pictureBox_preview.MouseMove += ContinueDrawing;
            pictureBox_preview.MouseUp += StopDrawing;

            pictureBox_preview.Image = drawingBitmap;
            Program.previewerInstance.pictureBox2.Image = drawingBitmap;
        }

        private void StartDrawing(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                isDrawing = true;

                // Push the current state of the bitmap to the undo stack
                SaveStateForUndo();

                // Clear the redo stack since a new action invalidates the redo history
                redoStack.Clear();

                lastDrawingPos = PreviewImagePosToDrawingPos(e.Location);

                UpdateUndoRedoButtonEnabled();
            }
        }

        private void StopDrawing(object sender, MouseEventArgs e)
        {
            isDrawing = false;
        }

        private void SaveStateForUndo()
        {
            // Create a clone of the current bitmap and push it to the undo stack
            undoStack.Push(new Bitmap(drawingBitmap));

            // Check if the undo stack exceeds the limit
            if (undoStack.Count > undoLimit)
            {
                // Remove the oldest state (convert to a queue-like behavior)
                var tempStack = new Stack<Bitmap>();
                while (undoStack.Count > undoLimit)
                {
                    tempStack.Push(undoStack.Pop());
                }
                tempStack.Pop(); // Remove the oldest item
                while (tempStack.Count > 0)
                {
                    undoStack.Push(tempStack.Pop());
                }
            }

            UpdateUndoRedoButtonEnabled();
        }

        public void Undo()
        {
            if (undoStack.Count > 0)
            {
                // Save the current state to the redo stack
                redoStack.Push(new Bitmap(drawingBitmap));

                // Restore the last state from the undo stack
                drawingBitmap = undoStack.Pop();
                drawingGraphics = Graphics.FromImage(drawingBitmap);

                // Update the PictureBox
                pictureBox_preview.Image = drawingBitmap;
                pictureBox_preview.Invalidate();
                Program.previewerInstance.pictureBox2.Image = drawingBitmap;
                Program.previewerInstance.pictureBox2.Invalidate();
            }

            UpdateUndoRedoButtonEnabled();
        }

        public void Redo()
        {
            if (redoStack.Count > 0)
            {
                // Save the current state to the undo stack
                undoStack.Push(new Bitmap(drawingBitmap));

                // Restore the last state from the redo stack
                drawingBitmap = redoStack.Pop();
                drawingGraphics = Graphics.FromImage(drawingBitmap);

                // Update the PictureBox
                pictureBox_preview.Image = drawingBitmap;
                pictureBox_preview.Invalidate();
                Program.previewerInstance.pictureBox2.Image = drawingBitmap;
                Program.previewerInstance.pictureBox2.Invalidate();
            }

            UpdateUndoRedoButtonEnabled();
        }

        public void UpdateUndoRedoButtonEnabled()
        {
            button_undoDraw.Enabled = undoStack.Count > 0;
            button_redoDraw.Enabled = redoStack.Count > 0;
        }


        private void ContinueDrawing(object sender, MouseEventArgs e)
        {
            if (isDrawing)
            {
                Point drawingPos = PreviewImagePosToDrawingPos(e.Location);
                int penWidth = trackBar_width.Value;

                if (eraseMode)
                {
                    // Erase with interpolation
                    using (Graphics g = Graphics.FromImage(drawingBitmap))
                    {
                        int steps = Math.Max(
                            Math.Abs(drawingPos.X - lastDrawingPos.X),
                            Math.Abs(drawingPos.Y - lastDrawingPos.Y)
                        ) / 10;

                        for (int i = 0; i <= steps; i++)
                        {
                            float t = steps > 0 ? (float)i / steps : 0;
                            int x = (int)(lastDrawingPos.X + t * (drawingPos.X - lastDrawingPos.X));
                            int y = (int)(lastDrawingPos.Y + t * (drawingPos.Y - lastDrawingPos.Y));

                            var eraseArea = new Rectangle(
                                x - penWidth / 2,
                                y - penWidth / 2,
                                penWidth,
                                penWidth
                            );

                            g.DrawImage(originalImage, eraseArea, eraseArea, GraphicsUnit.Pixel);
                        }
                    }
                }
                else
                {
                    // Draw with your original method
                    var drawArea = new Rectangle(
                        drawingPos.X - penWidth / 4,
                        drawingPos.Y - penWidth / 4,
                        penWidth / 2,
                        penWidth / 2
                    );

                    using (var rectPen = new Pen(selectedColor, penWidth / 2))
                    using (var linePen = new Pen(selectedColor, penWidth))
                    {
                        drawingGraphics.DrawLine(linePen, lastDrawingPos, drawingPos); // Draw the line
                        drawingGraphics.DrawEllipse(rectPen, drawArea); // Clean junctions
                    }
                }

                int invalidateRegionMargin = 30;
                // Calculate the bounding rectangle in control space, add some margin
                var invalidateRegion = new Rectangle(
                    Math.Min(lastControlPos.X, e.Location.X) - penWidth/2 - invalidateRegionMargin/2,
                    Math.Min(lastControlPos.Y, e.Location.Y) - penWidth/2 - invalidateRegionMargin/2,
                    Math.Abs(e.Location.X - lastControlPos.X) + penWidth + invalidateRegionMargin,
                    Math.Abs(e.Location.Y - lastControlPos.Y) + penWidth + invalidateRegionMargin
                );

                // Debug of invalidation zones
                //DebugDrawRect(invalidateRegion, pictureBox_preview);

                // Invalidate the cumulative region
                pictureBox_preview.Invalidate(invalidateRegion);

                var fullscreenInvalidateRegion = ConvertInvalidateRegion(invalidateRegion, pictureBox_preview, Program.previewerInstance.pictureBox2);
                Program.previewerInstance.pictureBox2.Invalidate(fullscreenInvalidateRegion);

                //DebugDrawRect(fullscreenInvalidateRegion, Program.previewerInstance.pictureBox2);

                lastDrawingPos = drawingPos; // Update the last drawing position
                lastControlPos = e.Location; // Update the last control position
            }
        }

        private Rectangle ConvertInvalidateRegion(Rectangle invalidateRegion, Control sourceControl, Control targetControl)
        {
            // Calculate the scale factors
            float scaleX = (float)targetControl.Width / sourceControl.Width;
            float scaleY = (float)targetControl.Height / sourceControl.Height;

            // Scale and adjust the rectangle
            return new Rectangle(
                (int)(invalidateRegion.X * scaleX),
                (int)(invalidateRegion.Y * scaleY),
                (int)(invalidateRegion.Width * scaleX),
                (int)(invalidateRegion.Height * scaleY)
            );
        }


        private void DebugDrawRect(Rectangle rect, Control control)
        {
            using (Graphics g = control.CreateGraphics())
            {
                using (Pen debugPen = new Pen(Color.Green, 2))
                {
                    g.DrawRectangle(debugPen, rect);
                }
            }
        }


        private Point PreviewImagePosToDrawingPos(Point PreviewImagePos)
        {
            PointF normalizedPos = NormalizePreviewImagePos(new PointF(PreviewImagePos.X, PreviewImagePos.Y));
            normalizedPos.X *= drawingBitmap.Size.Width;
            normalizedPos.Y *= drawingBitmap.Size.Height;
            
            return new Point((int)normalizedPos.X, (int)normalizedPos.Y);
        }

        private string CustomColorsFile =>
    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "RpgScreenCaster", "custom_colors.json");
        private int[] customColors = new int[16]; // Default custom colors (16 slots)

        private void EnsureDirectoryExists()
        {
            string directory = Path.GetDirectoryName(CustomColorsFile);
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }

        // Load custom colors from file
        private void LoadCustomColors()
        {
            if (File.Exists(CustomColorsFile))
            {
                try
                {
                    string json = File.ReadAllText(CustomColorsFile);
                    customColors = System.Text.Json.JsonSerializer.Deserialize<int[]>(json);
                }
                catch
                {
                    // Handle errors (e.g., file corrupted or invalid format)
                    customColors = new int[16]; // Reset to default
                }
            }
        }

        // Save custom colors to file
        private void SaveCustomColors()
        {
            try
            {
                EnsureDirectoryExists(); // Ensure the directory exists

                string json = System.Text.Json.JsonSerializer.Serialize(customColors);
                File.WriteAllText(CustomColorsFile, json);
            }
            catch
            {
                // Handle errors (e.g., file write access issues)
            }
        }

        private void ClickColorPicker(object sender, EventArgs e)
        {
            using (ColorDialog colorDialog = new ColorDialog())
            {
                colorDialog.AllowFullOpen = true;
                colorDialog.AnyColor = true;
                colorDialog.SolidColorOnly = false;
                colorDialog.CustomColors = customColors;

                if (colorDialog.ShowDialog() == DialogResult.OK)
                {
                    selectedColor = colorDialog.Color;
                    customColors = colorDialog.CustomColors;
                    panel_selectedColor.BackColor = selectedColor;
                    SaveCustomColors();
                }
            }
        }

        private void ClickDrawMode(object sender, EventArgs e)
        {
            eraseMode = false;
            button_pen.Enabled = eraseMode;
            button_eraser.Enabled = !eraseMode;
        }

        private void ClickEraseMode(object sender, EventArgs e)
        {
            eraseMode = true;
            button_pen.Enabled = eraseMode;
            button_eraser.Enabled = !eraseMode;
        }

        private void button_undoDraw_Click(object sender, EventArgs e)
        {
            Undo();
        }

        private void button_redoDraw_Click(object sender, EventArgs e)
        {
            Redo();
        }
    }
}
