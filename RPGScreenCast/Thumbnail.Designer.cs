﻿
namespace RPGScreenCast
{
    partial class Thumbnail
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_fileName = new System.Windows.Forms.Label();
            this.pictureBox_fileType = new System.Windows.Forms.PictureBox();
            this.pictureBox_thumbnail = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_fileType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_thumbnail)).BeginInit();
            this.SuspendLayout();
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.label_fileName);
            this.panel1.Controls.Add(this.pictureBox_fileType);
            this.panel1.Controls.Add(this.pictureBox_thumbnail);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(150, 150);
            this.panel1.TabIndex = 0;
            // 
            // label_fileName
            // 
            this.label_fileName.AutoEllipsis = true;
            this.label_fileName.AutoSize = true;
            this.label_fileName.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label_fileName.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label_fileName.Location = new System.Drawing.Point(0, 129);
            this.label_fileName.MaximumSize = new System.Drawing.Size(145, 0);
            this.label_fileName.Name = "label_fileName";
            this.label_fileName.Padding = new System.Windows.Forms.Padding(3);
            this.label_fileName.Size = new System.Drawing.Size(69, 19);
            this.label_fileName.TabIndex = 3;
            this.label_fileName.Text = "label1zdqzd";
            this.label_fileName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox_fileType
            // 
            this.pictureBox_fileType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_fileType.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_fileType.Image = global::RPGScreenCast.Properties.Resources.fileTypePdf;
            this.pictureBox_fileType.Location = new System.Drawing.Point(115, 3);
            this.pictureBox_fileType.Name = "pictureBox_fileType";
            this.pictureBox_fileType.Size = new System.Drawing.Size(30, 30);
            this.pictureBox_fileType.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_fileType.TabIndex = 2;
            this.pictureBox_fileType.TabStop = false;
            // 
            // pictureBox_thumbnail
            // 
            this.pictureBox_thumbnail.BackColor = System.Drawing.Color.Silver;
            this.pictureBox_thumbnail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_thumbnail.Location = new System.Drawing.Point(0, 0);
            this.pictureBox_thumbnail.Name = "pictureBox_thumbnail";
            this.pictureBox_thumbnail.Size = new System.Drawing.Size(148, 148);
            this.pictureBox_thumbnail.TabIndex = 1;
            this.pictureBox_thumbnail.TabStop = false;
            this.pictureBox_thumbnail.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.OnDoubleClick);
            this.pictureBox_thumbnail.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnSimpleClick);
            // 
            // Thumbnail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "Thumbnail";
            this.Load += new System.EventHandler(this.Thumbnail_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_fileType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_thumbnail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox_thumbnail;
        private System.Windows.Forms.PictureBox pictureBox_fileType;
        private System.Windows.Forms.Label label_fileName;
    }
}
