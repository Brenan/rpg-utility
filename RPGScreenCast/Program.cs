﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace RPGScreenCast
{
    static class Program
    {

        static Form1 mainWindowInstance;
        public static FullScreenWindow previewerInstance;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            previewerInstance = new FullScreenWindow();
            mainWindowInstance = new Form1();

            Application.Run(new MultiFormContext(mainWindowInstance, previewerInstance));
        }

        public static void ImageSelected(Image img, Thumbnail thumbnail)
        {
            mainWindowInstance.UpdatePreview(img, thumbnail);
            mainWindowInstance.SetCurrentPdfPage(0);
        }
    }
}
