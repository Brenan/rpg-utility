﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RPGScreenCast
{
    public partial class FullScreenWindow : Form
    {
        public bool isFullscreen;
        bool hasMouseMoved;
        Point lastMouseLocation;
        public FullScreenWindow()
        {
            InitializeComponent();
            pointer.Visible = false;
            pictureBox2.Controls.Add(pictureBox1);
            pictureBox2.Controls.Add(pointer);
        }

        private void FullScreenWindow_Load(object sender, EventArgs e)
        {

        }

        public void UpdateImage(Image img)
        {
            pictureBox2.Image = img;
            pictureBox2.Update();
        }

        public void GoFullscreen(bool fullscreen)
        {
            if (fullscreen)
            {
                this.WindowState = FormWindowState.Normal;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                this.Bounds = Screen.FromControl(this).Bounds;
                
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable;
                pictureBox1.Visible = true;
            }
            isFullscreen = fullscreen;
        }

        public void PointOnImage(PointF normalizedPos)
        {
            //pointer.Location = new Point((int)((normalizedPos.X * pictureBox2.Width) - pointer.Width / 2f), (int)((normalizedPos.Y * pictureBox2.Height) - pointer.Height / 2f));

            float imageAspect = (float)pictureBox2.Image.Width / pictureBox2.Image.Height;
            float boxAspect = (float)pictureBox2.Width / pictureBox2.Height;
            PointF imageSize;
            if (imageAspect < boxAspect)
                imageSize = new PointF(imageAspect * pictureBox2.Height, pictureBox2.Height);
            else if (imageAspect > boxAspect)
                imageSize = new PointF(pictureBox2.Width, pictureBox2.Width / imageAspect);
            else
                imageSize = new PointF(pictureBox2.Width, pictureBox2.Height);

            PointF borderSize = new PointF((pictureBox2.Width - imageSize.X) / 2f, (pictureBox2.Height - imageSize.Y) / 2f);

            pointer.Location = new Point((int)(normalizedPos.X * imageSize.X + borderSize.X - pictureBox1.Width/2f), (int)(normalizedPos.Y * imageSize.Y + borderSize.Y - pictureBox1.Height / 2f));

            pictureBox2.Update();
            pointer.Update();
            Update();
        }

        public void UpdatePointerVisiblity(bool visible)
        {
            pointer.Visible = visible;
        }

        public void ClickFullscreen(object sender, EventArgs e)
        {
            GoFullscreen(!isFullscreen);
            pictureBox1.Image = new Bitmap(isFullscreen ? Properties.Resources.full_screen_exit_icon_icons_com_72743 : Properties.Resources.iconfinder_fullscreen_326650);

            WaitSomeTime();
        }

        private void MouseMoved(object sender, MouseEventArgs e)
        {
            if(e.Location != lastMouseLocation && isFullscreen)
            {
                pictureBox1.Visible = true;
                hasMouseMoved = true;
                WaitSomeTime();
                lastMouseLocation = e.Location;
            }
        }

        public async void WaitSomeTime()
        {
            hasMouseMoved = false;
            Task delay = Task.Delay(2000);
            await delay;

            if (isFullscreen)
                pictureBox1.Visible = hasMouseMoved;
        }
    }
}
