﻿using MuPDFCore;
using RPGScreenCast.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace RPGScreenCast
{
    public partial class Thumbnail : UserControl
    {
        public Thumbnail()
        {
            InitializeComponent();
        }

        public string FilePath;
        public string FavPath;
        public Image ThumbImg;
        public Image Img;
        public List<Image> PdfPages = new List<Image>();
        public bool isFavorite;

        public Thumbnail(string file, bool favorite, bool isFavPath)
        {
            InitializeComponent();

            panel1.Controls.Add(pictureBox_thumbnail);
            pictureBox_thumbnail.Controls.Add(pictureBox_fileType);

            FilePath = file;

            label_fileName.Text = Path.GetFileNameWithoutExtension(file);

            if(file.Contains(".pdf"))
            {
                //Initialise the MuPDF context. This is needed to open or create documents.
                using (MuPDFContext ctx = new MuPDFContext())
                {
                    //Open a PDF document
                    using (MuPDFDocument document = new MuPDFDocument(ctx, file))
                    {
                        //Page index (page 0 is the first page of the document)
                        int pageIndex = 0;

                        //Save the first page as a PNG image with transparency, at a 1x zoom level (1pt = 1px).

                        using (Stream imgStream = new FileStream("temp.jpeg", FileMode.OpenOrCreate))
                        {
                            document.WriteImage(pageIndex, 0.25f, PixelFormats.RGB, imgStream, RasterOutputFileTypes.JPEG);
                            ThumbImg = new Bitmap(imgStream);
                        }
                    }
                }

                pictureBox_fileType.Image = Properties.Resources.fileTypePdf;
            }
            else
            {
                using (var bmpTemp = new Bitmap(FilePath))
                {
                    ThumbImg = Img = new Bitmap(bmpTemp);
                }
                pictureBox_fileType.Image = Properties.Resources.fileTypeImg;
            }

            //Img = Image.FromFile(file);
            isFavorite = favorite;

            if (isFavPath) FavPath = FilePath;

            pictureBox_thumbnail.Height = this.Height;
            pictureBox_thumbnail.Width = this.Width;
            pictureBox_thumbnail.SizeMode = PictureBoxSizeMode.Zoom;
            SetImage(ThumbImg);
        }


        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void Thumbnail_Load(object sender, EventArgs e)
        {

        }

        public void SetImage(Image img)
        {

            pictureBox_thumbnail.Image = img;
            pictureBox_thumbnail.Update();
        }

        public void RemoveFromFavs()
        {
            /*this.Dispose();
            File.Delete(FilePath);*/
        }

        private void OnSimpleClick(object sender, MouseEventArgs e)
        {
            if (e.Clicks > 1)
                RaiseMouseEvent(sender, e);
            else
                DoDragDrop(this, DragDropEffects.Move);
        }

        private void OnDoubleClick(object sender, MouseEventArgs e)
        {
            if (FilePath.Contains(".pdf"))
            {
                if (PdfPages.Count == 0)
                {
                    //Initialise the MuPDF context. This is needed to open or create documents.
                    using (MuPDFContext ctx = new MuPDFContext())
                    {
                        //Open a PDF document
                        using (MuPDFDocument document = new MuPDFDocument(ctx, FilePath))
                        {
                            for (int pageIndex = 0; pageIndex < document.Pages.Count; pageIndex++)
                            {
                                float zoom = 2.0f;
                                using (Stream imgStream = new FileStream("temp.jpeg", FileMode.OpenOrCreate))
                                {
                                    document.WriteImage(pageIndex, zoom, PixelFormats.RGB, imgStream, RasterOutputFileTypes.JPEG);
                                    PdfPages.Add(new Bitmap(imgStream));
                                }
                            }
                        }
                    }
                }
                if (PdfPages.Count > 0)
                {
                    Form1.currentPdfPage = 0;
                    Program.ImageSelected(PdfPages[0], this);
                }

            }
            else
                Program.ImageSelected(Img, this);
        }
    }
}


