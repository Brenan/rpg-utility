﻿
namespace RPGScreenCast
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            this.splitContainer5 = new System.Windows.Forms.SplitContainer();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.splitContainer6 = new System.Windows.Forms.SplitContainer();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.splitContainer_mainH = new System.Windows.Forms.SplitContainer();
            this.splitContainer_leftV = new System.Windows.Forms.SplitContainer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel_searchContainer = new System.Windows.Forms.TableLayoutPanel();
            this.label_search = new System.Windows.Forms.Label();
            this.searchBox = new System.Windows.Forms.TextBox();
            this.button_search = new System.Windows.Forms.Button();
            this.button_clearSearch = new System.Windows.Forms.Button();
            this.searchResult = new System.Windows.Forms.Label();
            this.progressBar_search = new System.Windows.Forms.ProgressBar();
            this.panel_switcher_search_tree = new System.Windows.Forms.Panel();
            this.flowLayoutPanel_searchResult = new System.Windows.Forms.FlowLayoutPanel();
            this.treeView_dirs = new System.Windows.Forms.TreeView();
            this.panel_dirContent = new System.Windows.Forms.Panel();
            this.panel_loadbar = new System.Windows.Forms.Panel();
            this.progressBar_dirContent = new System.Windows.Forms.ProgressBar();
            this.flowLayoutPanel_dirContent = new System.Windows.Forms.FlowLayoutPanel();
            this.splitContainer_rightV = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox_preview = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.button_eraser = new System.Windows.Forms.Button();
            this.button_pen = new System.Windows.Forms.Button();
            this.button_colorpicker = new System.Windows.Forms.Button();
            this.panel_selectedColor = new System.Windows.Forms.Panel();
            this.trackBar_width = new System.Windows.Forms.TrackBar();
            this.button_point = new System.Windows.Forms.Button();
            this.label_width = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button_undoDraw = new System.Windows.Forms.Button();
            this.button_redoDraw = new System.Windows.Forms.Button();
            this.flowpanel_pdfPages = new System.Windows.Forms.FlowLayoutPanel();
            this.button_nextPage = new System.Windows.Forms.Button();
            this.label_pdfPage = new System.Windows.Forms.Label();
            this.button_prevPage = new System.Windows.Forms.Button();
            this.tableLayoutPanel_favContainer = new System.Windows.Forms.TableLayoutPanel();
            this.panel_favBar = new System.Windows.Forms.Panel();
            this.button_loadFav = new System.Windows.Forms.Button();
            this.button_saveFav = new System.Windows.Forms.Button();
            this.flowLayoutPanel_favsContent = new System.Windows.Forms.FlowLayoutPanel();
            this.button_loadDir = new System.Windows.Forms.Button();
            this.label_dirPath = new System.Windows.Forms.Label();
            this.button_fullScreen = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).BeginInit();
            this.splitContainer5.Panel1.SuspendLayout();
            this.splitContainer5.Panel2.SuspendLayout();
            this.splitContainer5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).BeginInit();
            this.splitContainer6.Panel1.SuspendLayout();
            this.splitContainer6.Panel2.SuspendLayout();
            this.splitContainer6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_mainH)).BeginInit();
            this.splitContainer_mainH.Panel1.SuspendLayout();
            this.splitContainer_mainH.Panel2.SuspendLayout();
            this.splitContainer_mainH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_leftV)).BeginInit();
            this.splitContainer_leftV.Panel1.SuspendLayout();
            this.splitContainer_leftV.Panel2.SuspendLayout();
            this.splitContainer_leftV.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel_searchContainer.SuspendLayout();
            this.panel_switcher_search_tree.SuspendLayout();
            this.panel_dirContent.SuspendLayout();
            this.panel_loadbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_rightV)).BeginInit();
            this.splitContainer_rightV.Panel1.SuspendLayout();
            this.splitContainer_rightV.Panel2.SuspendLayout();
            this.splitContainer_rightV.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_preview)).BeginInit();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_width)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowpanel_pdfPages.SuspendLayout();
            this.tableLayoutPanel_favContainer.SuspendLayout();
            this.panel_favBar.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(0, 0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(200, 100);
            this.tabPage2.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer4.Location = new System.Drawing.Point(6, 34);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.splitContainer5);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.splitContainer6);
            this.splitContainer4.Size = new System.Drawing.Size(1102, 475);
            this.splitContainer4.SplitterDistance = 418;
            this.splitContainer4.TabIndex = 10;
            // 
            // splitContainer5
            // 
            this.splitContainer5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer5.Location = new System.Drawing.Point(0, 0);
            this.splitContainer5.Name = "splitContainer5";
            this.splitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer5.Panel1
            // 
            this.splitContainer5.Panel1.Controls.Add(this.treeView2);
            // 
            // splitContainer5.Panel2
            // 
            this.splitContainer5.Panel2.Controls.Add(this.flowLayoutPanel3);
            this.splitContainer5.Size = new System.Drawing.Size(418, 475);
            this.splitContainer5.SplitterDistance = 87;
            this.splitContainer5.TabIndex = 7;
            // 
            // treeView2
            // 
            this.treeView2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.treeView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView2.Location = new System.Drawing.Point(0, 0);
            this.treeView2.Margin = new System.Windows.Forms.Padding(10);
            this.treeView2.Name = "treeView2";
            this.treeView2.Size = new System.Drawing.Size(418, 87);
            this.treeView2.TabIndex = 6;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.AllowDrop = true;
            this.flowLayoutPanel3.AutoScroll = true;
            this.flowLayoutPanel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(418, 384);
            this.flowLayoutPanel3.TabIndex = 0;
            // 
            // splitContainer6
            // 
            this.splitContainer6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer6.Location = new System.Drawing.Point(0, 0);
            this.splitContainer6.Name = "splitContainer6";
            this.splitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer6.Panel1
            // 
            this.splitContainer6.Panel1.Controls.Add(this.pictureBox3);
            // 
            // splitContainer6.Panel2
            // 
            this.splitContainer6.Panel2.Controls.Add(this.flowLayoutPanel4);
            this.splitContainer6.Size = new System.Drawing.Size(680, 475);
            this.splitContainer6.SplitterDistance = 144;
            this.splitContainer6.TabIndex = 8;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox3.Location = new System.Drawing.Point(0, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(680, 144);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.AllowDrop = true;
            this.flowLayoutPanel4.AutoScroll = true;
            this.flowLayoutPanel4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flowLayoutPanel4.Size = new System.Drawing.Size(680, 327);
            this.flowLayoutPanel4.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(113, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(266, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Tu n\'as pas encore sélectionné de dossier sale merde !";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.DimGray;
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Controls.Add(this.splitContainer_mainH);
            this.tabPage1.Controls.Add(this.button_loadDir);
            this.tabPage1.Controls.Add(this.label_dirPath);
            this.tabPage1.Controls.Add(this.button_fullScreen);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1317, 674);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Screen Caster";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Silver;
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Image = global::RPGScreenCast.Properties.Resources.trash;
            this.pictureBox2.Location = new System.Drawing.Point(1263, 609);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 51);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.OnTrashButtonClick);
            // 
            // splitContainer_mainH
            // 
            this.splitContainer_mainH.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer_mainH.Location = new System.Drawing.Point(6, 34);
            this.splitContainer_mainH.Name = "splitContainer_mainH";
            // 
            // splitContainer_mainH.Panel1
            // 
            this.splitContainer_mainH.Panel1.Controls.Add(this.splitContainer_leftV);
            // 
            // splitContainer_mainH.Panel2
            // 
            this.splitContainer_mainH.Panel2.Controls.Add(this.splitContainer_rightV);
            this.splitContainer_mainH.Size = new System.Drawing.Size(1305, 634);
            this.splitContainer_mainH.SplitterDistance = 518;
            this.splitContainer_mainH.TabIndex = 9;
            // 
            // splitContainer_leftV
            // 
            this.splitContainer_leftV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_leftV.Location = new System.Drawing.Point(0, 0);
            this.splitContainer_leftV.Name = "splitContainer_leftV";
            this.splitContainer_leftV.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_leftV.Panel1
            // 
            this.splitContainer_leftV.Panel1.Controls.Add(this.panel1);
            // 
            // splitContainer_leftV.Panel2
            // 
            this.splitContainer_leftV.Panel2.Controls.Add(this.panel_dirContent);
            this.splitContainer_leftV.Size = new System.Drawing.Size(518, 634);
            this.splitContainer_leftV.SplitterDistance = 270;
            this.splitContainer_leftV.TabIndex = 7;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel_searchContainer);
            this.panel1.Controls.Add(this.panel_switcher_search_tree);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(518, 270);
            this.panel1.TabIndex = 8;
            // 
            // tableLayoutPanel_searchContainer
            // 
            this.tableLayoutPanel_searchContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel_searchContainer.ColumnCount = 4;
            this.tableLayoutPanel_searchContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.33333F));
            this.tableLayoutPanel_searchContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.66666F));
            this.tableLayoutPanel_searchContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel_searchContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel_searchContainer.Controls.Add(this.label_search, 0, 0);
            this.tableLayoutPanel_searchContainer.Controls.Add(this.searchBox, 1, 0);
            this.tableLayoutPanel_searchContainer.Controls.Add(this.button_search, 2, 0);
            this.tableLayoutPanel_searchContainer.Controls.Add(this.button_clearSearch, 3, 0);
            this.tableLayoutPanel_searchContainer.Controls.Add(this.searchResult, 0, 1);
            this.tableLayoutPanel_searchContainer.Controls.Add(this.progressBar_search, 1, 1);
            this.tableLayoutPanel_searchContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel_searchContainer.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel_searchContainer.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel_searchContainer.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel_searchContainer.MaximumSize = new System.Drawing.Size(0, 40);
            this.tableLayoutPanel_searchContainer.MinimumSize = new System.Drawing.Size(0, 40);
            this.tableLayoutPanel_searchContainer.Name = "tableLayoutPanel_searchContainer";
            this.tableLayoutPanel_searchContainer.RowCount = 2;
            this.tableLayoutPanel_searchContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_searchContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_searchContainer.Size = new System.Drawing.Size(518, 40);
            this.tableLayoutPanel_searchContainer.TabIndex = 7;
            // 
            // label_search
            // 
            this.label_search.AutoSize = true;
            this.label_search.Dock = System.Windows.Forms.DockStyle.Left;
            this.label_search.Location = new System.Drawing.Point(3, 0);
            this.label_search.Name = "label_search";
            this.label_search.Size = new System.Drawing.Size(63, 20);
            this.label_search.TabIndex = 0;
            this.label_search.Text = "Rechercher";
            this.label_search.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // searchBox
            // 
            this.searchBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchBox.Location = new System.Drawing.Point(122, 3);
            this.searchBox.Name = "searchBox";
            this.searchBox.Size = new System.Drawing.Size(344, 20);
            this.searchBox.TabIndex = 1;
            this.searchBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyPressSearch);
            // 
            // button_search
            // 
            this.button_search.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_search.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_search.Location = new System.Drawing.Point(469, 0);
            this.button_search.Margin = new System.Windows.Forms.Padding(0);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(22, 20);
            this.button_search.TabIndex = 2;
            this.button_search.Text = "🔎";
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.ClickSearch);
            // 
            // button_clearSearch
            // 
            this.button_clearSearch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_clearSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_clearSearch.Location = new System.Drawing.Point(491, 0);
            this.button_clearSearch.Margin = new System.Windows.Forms.Padding(0);
            this.button_clearSearch.Name = "button_clearSearch";
            this.button_clearSearch.Size = new System.Drawing.Size(27, 20);
            this.button_clearSearch.TabIndex = 4;
            this.button_clearSearch.Text = "❌";
            this.button_clearSearch.UseVisualStyleBackColor = true;
            this.button_clearSearch.Click += new System.EventHandler(this.ClickClearSearch);
            // 
            // searchResult
            // 
            this.searchResult.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.searchResult.AutoSize = true;
            this.searchResult.Location = new System.Drawing.Point(3, 23);
            this.searchResult.Name = "searchResult";
            this.searchResult.Size = new System.Drawing.Size(54, 13);
            this.searchResult.TabIndex = 3;
            this.searchResult.Text = "x résultats";
            this.searchResult.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.searchResult.Visible = false;
            // 
            // progressBar_search
            // 
            this.progressBar_search.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar_search.Location = new System.Drawing.Point(122, 23);
            this.progressBar_search.Name = "progressBar_search";
            this.progressBar_search.Size = new System.Drawing.Size(344, 14);
            this.progressBar_search.Step = 100;
            this.progressBar_search.TabIndex = 5;
            this.progressBar_search.Visible = false;
            // 
            // panel_switcher_search_tree
            // 
            this.panel_switcher_search_tree.Controls.Add(this.flowLayoutPanel_searchResult);
            this.panel_switcher_search_tree.Controls.Add(this.treeView_dirs);
            this.panel_switcher_search_tree.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_switcher_search_tree.Location = new System.Drawing.Point(0, 0);
            this.panel_switcher_search_tree.Margin = new System.Windows.Forms.Padding(0);
            this.panel_switcher_search_tree.Name = "panel_switcher_search_tree";
            this.panel_switcher_search_tree.Padding = new System.Windows.Forms.Padding(0, 40, 0, 0);
            this.panel_switcher_search_tree.Size = new System.Drawing.Size(518, 270);
            this.panel_switcher_search_tree.TabIndex = 0;
            // 
            // flowLayoutPanel_searchResult
            // 
            this.flowLayoutPanel_searchResult.AllowDrop = true;
            this.flowLayoutPanel_searchResult.AutoScroll = true;
            this.flowLayoutPanel_searchResult.AutoSize = true;
            this.flowLayoutPanel_searchResult.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel_searchResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel_searchResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_searchResult.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel_searchResult.Location = new System.Drawing.Point(0, 40);
            this.flowLayoutPanel_searchResult.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel_searchResult.Name = "flowLayoutPanel_searchResult";
            this.flowLayoutPanel_searchResult.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flowLayoutPanel_searchResult.Size = new System.Drawing.Size(518, 230);
            this.flowLayoutPanel_searchResult.TabIndex = 7;
            this.flowLayoutPanel_searchResult.Visible = false;
            // 
            // treeView_dirs
            // 
            this.treeView_dirs.BackColor = System.Drawing.SystemColors.ControlDark;
            this.treeView_dirs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView_dirs.Location = new System.Drawing.Point(0, 40);
            this.treeView_dirs.Margin = new System.Windows.Forms.Padding(0);
            this.treeView_dirs.Name = "treeView_dirs";
            this.treeView_dirs.Size = new System.Drawing.Size(518, 230);
            this.treeView_dirs.TabIndex = 6;
            this.treeView_dirs.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.treeView_dirs.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.OnClickedFolderNode);
            // 
            // panel_dirContent
            // 
            this.panel_dirContent.Controls.Add(this.panel_loadbar);
            this.panel_dirContent.Controls.Add(this.flowLayoutPanel_dirContent);
            this.panel_dirContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_dirContent.Location = new System.Drawing.Point(0, 0);
            this.panel_dirContent.Name = "panel_dirContent";
            this.panel_dirContent.Size = new System.Drawing.Size(518, 360);
            this.panel_dirContent.TabIndex = 2;
            // 
            // panel_loadbar
            // 
            this.panel_loadbar.AutoSize = true;
            this.panel_loadbar.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel_loadbar.Controls.Add(this.progressBar_dirContent);
            this.panel_loadbar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_loadbar.Location = new System.Drawing.Point(0, 327);
            this.panel_loadbar.Name = "panel_loadbar";
            this.panel_loadbar.Padding = new System.Windows.Forms.Padding(5);
            this.panel_loadbar.Size = new System.Drawing.Size(518, 33);
            this.panel_loadbar.TabIndex = 0;
            this.panel_loadbar.Visible = false;
            // 
            // progressBar_dirContent
            // 
            this.progressBar_dirContent.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.progressBar_dirContent.Location = new System.Drawing.Point(5, 5);
            this.progressBar_dirContent.Margin = new System.Windows.Forms.Padding(0);
            this.progressBar_dirContent.Name = "progressBar_dirContent";
            this.progressBar_dirContent.Size = new System.Drawing.Size(508, 23);
            this.progressBar_dirContent.Step = 100;
            this.progressBar_dirContent.TabIndex = 0;
            // 
            // flowLayoutPanel_dirContent
            // 
            this.flowLayoutPanel_dirContent.AllowDrop = true;
            this.flowLayoutPanel_dirContent.AutoScroll = true;
            this.flowLayoutPanel_dirContent.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel_dirContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel_dirContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_dirContent.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel_dirContent.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel_dirContent.Name = "flowLayoutPanel_dirContent";
            this.flowLayoutPanel_dirContent.Size = new System.Drawing.Size(518, 360);
            this.flowLayoutPanel_dirContent.TabIndex = 1;
            this.flowLayoutPanel_dirContent.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnMainDragDrop);
            this.flowLayoutPanel_dirContent.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnMainDragEnter);
            // 
            // splitContainer_rightV
            // 
            this.splitContainer_rightV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_rightV.Location = new System.Drawing.Point(0, 0);
            this.splitContainer_rightV.Name = "splitContainer_rightV";
            this.splitContainer_rightV.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_rightV.Panel1
            // 
            this.splitContainer_rightV.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer_rightV.Panel2
            // 
            this.splitContainer_rightV.Panel2.Controls.Add(this.tableLayoutPanel_favContainer);
            this.splitContainer_rightV.Size = new System.Drawing.Size(783, 634);
            this.splitContainer_rightV.SplitterDistance = 366;
            this.splitContainer_rightV.TabIndex = 8;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox_preview, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(783, 366);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // pictureBox_preview
            // 
            this.pictureBox_preview.BackColor = System.Drawing.SystemColors.ControlDark;
            this.pictureBox_preview.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox_preview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_preview.Image = global::RPGScreenCast.Properties.Resources.otter;
            this.pictureBox_preview.Location = new System.Drawing.Point(3, 3);
            this.pictureBox_preview.Name = "pictureBox_preview";
            this.pictureBox_preview.Size = new System.Drawing.Size(627, 360);
            this.pictureBox_preview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_preview.TabIndex = 2;
            this.pictureBox_preview.TabStop = false;
            this.pictureBox_preview.MouseMove += new System.Windows.Forms.MouseEventHandler(this.OnPointing);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(636, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(144, 360);
            this.panel2.TabIndex = 3;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.trackBar_width, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.button_point, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label_width, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowpanel_pdfPages, 0, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 7;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(144, 360);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.button_eraser, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.button_pen, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.button_colorpicker, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel_selectedColor, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 41);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(138, 35);
            this.tableLayoutPanel3.TabIndex = 11;
            // 
            // button_eraser
            // 
            this.button_eraser.Image = ((System.Drawing.Image)(resources.GetObject("button_eraser.Image")));
            this.button_eraser.Location = new System.Drawing.Point(105, 3);
            this.button_eraser.Name = "button_eraser";
            this.button_eraser.Size = new System.Drawing.Size(30, 30);
            this.button_eraser.TabIndex = 7;
            this.button_eraser.UseVisualStyleBackColor = true;
            this.button_eraser.Click += new System.EventHandler(this.ClickEraseMode);
            // 
            // button_pen
            // 
            this.button_pen.Enabled = false;
            this.button_pen.Image = global::RPGScreenCast.Properties.Resources.pen;
            this.button_pen.Location = new System.Drawing.Point(71, 3);
            this.button_pen.Name = "button_pen";
            this.button_pen.Size = new System.Drawing.Size(28, 30);
            this.button_pen.TabIndex = 8;
            this.button_pen.UseVisualStyleBackColor = true;
            this.button_pen.Click += new System.EventHandler(this.ClickDrawMode);
            // 
            // button_colorpicker
            // 
            this.button_colorpicker.Image = global::RPGScreenCast.Properties.Resources.color_wheel;
            this.button_colorpicker.Location = new System.Drawing.Point(37, 3);
            this.button_colorpicker.Name = "button_colorpicker";
            this.button_colorpicker.Size = new System.Drawing.Size(28, 30);
            this.button_colorpicker.TabIndex = 9;
            this.button_colorpicker.UseVisualStyleBackColor = true;
            this.button_colorpicker.Click += new System.EventHandler(this.ClickColorPicker);
            // 
            // panel_selectedColor
            // 
            this.panel_selectedColor.BackColor = System.Drawing.Color.Black;
            this.panel_selectedColor.Location = new System.Drawing.Point(5, 5);
            this.panel_selectedColor.Margin = new System.Windows.Forms.Padding(5);
            this.panel_selectedColor.Name = "panel_selectedColor";
            this.panel_selectedColor.Padding = new System.Windows.Forms.Padding(5);
            this.panel_selectedColor.Size = new System.Drawing.Size(24, 25);
            this.panel_selectedColor.TabIndex = 10;
            // 
            // trackBar_width
            // 
            this.trackBar_width.Dock = System.Windows.Forms.DockStyle.Top;
            this.trackBar_width.Location = new System.Drawing.Point(3, 82);
            this.trackBar_width.Maximum = 100;
            this.trackBar_width.Minimum = 1;
            this.trackBar_width.Name = "trackBar_width";
            this.trackBar_width.Size = new System.Drawing.Size(138, 30);
            this.trackBar_width.TabIndex = 10;
            this.trackBar_width.Value = 20;
            // 
            // button_point
            // 
            this.button_point.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button_point.Location = new System.Drawing.Point(5, 334);
            this.button_point.Name = "button_point";
            this.button_point.Size = new System.Drawing.Size(133, 23);
            this.button_point.TabIndex = 5;
            this.button_point.Text = "Pointer";
            this.button_point.UseVisualStyleBackColor = true;
            this.button_point.Click += new System.EventHandler(this.ClickPointingButton);
            // 
            // label_width
            // 
            this.label_width.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label_width.AutoSize = true;
            this.label_width.Location = new System.Drawing.Point(45, 115);
            this.label_width.Name = "label_width";
            this.label_width.Size = new System.Drawing.Size(53, 13);
            this.label_width.TabIndex = 12;
            this.label_width.Text = "Épaisseur";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.flowLayoutPanel1.AutoSize = true;
            this.flowLayoutPanel1.Controls.Add(this.button_undoDraw);
            this.flowLayoutPanel1.Controls.Add(this.button_redoDraw);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(40, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(64, 29);
            this.flowLayoutPanel1.TabIndex = 13;
            // 
            // button_undoDraw
            // 
            this.button_undoDraw.Location = new System.Drawing.Point(3, 3);
            this.button_undoDraw.Name = "button_undoDraw";
            this.button_undoDraw.Size = new System.Drawing.Size(26, 23);
            this.button_undoDraw.TabIndex = 0;
            this.button_undoDraw.Text = "↩️";
            this.button_undoDraw.UseVisualStyleBackColor = true;
            this.button_undoDraw.Click += new System.EventHandler(this.button_undoDraw_Click);
            // 
            // button_redoDraw
            // 
            this.button_redoDraw.Location = new System.Drawing.Point(35, 3);
            this.button_redoDraw.Name = "button_redoDraw";
            this.button_redoDraw.Size = new System.Drawing.Size(26, 23);
            this.button_redoDraw.TabIndex = 1;
            this.button_redoDraw.Text = "↪️";
            this.button_redoDraw.UseVisualStyleBackColor = true;
            this.button_redoDraw.Click += new System.EventHandler(this.button_redoDraw_Click);
            // 
            // flowpanel_pdfPages
            // 
            this.flowpanel_pdfPages.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.flowpanel_pdfPages.AutoSize = true;
            this.flowpanel_pdfPages.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowpanel_pdfPages.Controls.Add(this.button_nextPage);
            this.flowpanel_pdfPages.Controls.Add(this.label_pdfPage);
            this.flowpanel_pdfPages.Controls.Add(this.button_prevPage);
            this.flowpanel_pdfPages.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowpanel_pdfPages.Location = new System.Drawing.Point(25, 293);
            this.flowpanel_pdfPages.Name = "flowpanel_pdfPages";
            this.flowpanel_pdfPages.Size = new System.Drawing.Size(94, 29);
            this.flowpanel_pdfPages.TabIndex = 6;
            this.flowpanel_pdfPages.Visible = false;
            // 
            // button_nextPage
            // 
            this.button_nextPage.Location = new System.Drawing.Point(68, 3);
            this.button_nextPage.Name = "button_nextPage";
            this.button_nextPage.Size = new System.Drawing.Size(23, 23);
            this.button_nextPage.TabIndex = 0;
            this.button_nextPage.Text = ">";
            this.button_nextPage.UseVisualStyleBackColor = true;
            this.button_nextPage.Click += new System.EventHandler(this.ClickNextPdfPage);
            // 
            // label_pdfPage
            // 
            this.label_pdfPage.AutoSize = true;
            this.label_pdfPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label_pdfPage.Location = new System.Drawing.Point(32, 0);
            this.label_pdfPage.Name = "label_pdfPage";
            this.label_pdfPage.Size = new System.Drawing.Size(30, 29);
            this.label_pdfPage.TabIndex = 2;
            this.label_pdfPage.Text = "1/12";
            this.label_pdfPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_prevPage
            // 
            this.button_prevPage.Location = new System.Drawing.Point(3, 3);
            this.button_prevPage.Name = "button_prevPage";
            this.button_prevPage.Size = new System.Drawing.Size(23, 23);
            this.button_prevPage.TabIndex = 1;
            this.button_prevPage.Text = "<";
            this.button_prevPage.UseVisualStyleBackColor = true;
            this.button_prevPage.Click += new System.EventHandler(this.ClickPreviousPdfPage);
            // 
            // tableLayoutPanel_favContainer
            // 
            this.tableLayoutPanel_favContainer.ColumnCount = 1;
            this.tableLayoutPanel_favContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_favContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_favContainer.Controls.Add(this.panel_favBar, 0, 0);
            this.tableLayoutPanel_favContainer.Controls.Add(this.flowLayoutPanel_favsContent, 0, 1);
            this.tableLayoutPanel_favContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_favContainer.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel_favContainer.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel_favContainer.Name = "tableLayoutPanel_favContainer";
            this.tableLayoutPanel_favContainer.RowCount = 2;
            this.tableLayoutPanel_favContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel_favContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 89.29664F));
            this.tableLayoutPanel_favContainer.Size = new System.Drawing.Size(783, 264);
            this.tableLayoutPanel_favContainer.TabIndex = 0;
            this.tableLayoutPanel_favContainer.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // panel_favBar
            // 
            this.panel_favBar.Controls.Add(this.button_loadFav);
            this.panel_favBar.Controls.Add(this.button_saveFav);
            this.panel_favBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_favBar.Location = new System.Drawing.Point(3, 3);
            this.panel_favBar.MaximumSize = new System.Drawing.Size(0, 29);
            this.panel_favBar.MinimumSize = new System.Drawing.Size(0, 29);
            this.panel_favBar.Name = "panel_favBar";
            this.panel_favBar.Size = new System.Drawing.Size(777, 29);
            this.panel_favBar.TabIndex = 0;
            // 
            // button_loadFav
            // 
            this.button_loadFav.Location = new System.Drawing.Point(107, 3);
            this.button_loadFav.Name = "button_loadFav";
            this.button_loadFav.Size = new System.Drawing.Size(75, 23);
            this.button_loadFav.TabIndex = 1;
            this.button_loadFav.Text = "Charger";
            this.button_loadFav.UseVisualStyleBackColor = true;
            this.button_loadFav.Click += new System.EventHandler(this.LoadFabButton);
            // 
            // button_saveFav
            // 
            this.button_saveFav.Location = new System.Drawing.Point(4, 3);
            this.button_saveFav.Name = "button_saveFav";
            this.button_saveFav.Size = new System.Drawing.Size(94, 23);
            this.button_saveFav.TabIndex = 0;
            this.button_saveFav.Text = "Sauvegarder";
            this.button_saveFav.UseVisualStyleBackColor = true;
            this.button_saveFav.Click += new System.EventHandler(this.SaveFavButton);
            // 
            // flowLayoutPanel_favsContent
            // 
            this.flowLayoutPanel_favsContent.AllowDrop = true;
            this.flowLayoutPanel_favsContent.AutoScroll = true;
            this.flowLayoutPanel_favsContent.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flowLayoutPanel_favsContent.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel_favsContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel_favsContent.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel_favsContent.Location = new System.Drawing.Point(3, 33);
            this.flowLayoutPanel_favsContent.Name = "flowLayoutPanel_favsContent";
            this.flowLayoutPanel_favsContent.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flowLayoutPanel_favsContent.Size = new System.Drawing.Size(777, 228);
            this.flowLayoutPanel_favsContent.TabIndex = 1;
            this.flowLayoutPanel_favsContent.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.OnThmbRemovedFromFavs);
            this.flowLayoutPanel_favsContent.DragDrop += new System.Windows.Forms.DragEventHandler(this.OnFavDragDrop);
            this.flowLayoutPanel_favsContent.DragEnter += new System.Windows.Forms.DragEventHandler(this.OnFavDragEnter);
            // 
            // button_loadDir
            // 
            this.button_loadDir.Location = new System.Drawing.Point(7, 5);
            this.button_loadDir.Name = "button_loadDir";
            this.button_loadDir.Size = new System.Drawing.Size(100, 23);
            this.button_loadDir.TabIndex = 2;
            this.button_loadDir.Text = "Le bon dossier";
            this.button_loadDir.UseVisualStyleBackColor = true;
            this.button_loadDir.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnClickedFolderSelection);
            // 
            // label_dirPath
            // 
            this.label_dirPath.AutoSize = true;
            this.label_dirPath.ForeColor = System.Drawing.SystemColors.Control;
            this.label_dirPath.Location = new System.Drawing.Point(113, 10);
            this.label_dirPath.Name = "label_dirPath";
            this.label_dirPath.Size = new System.Drawing.Size(212, 13);
            this.label_dirPath.TabIndex = 3;
            this.label_dirPath.Text = "Tu n\'as pas encore sélectionné de dossier !";
            // 
            // button_fullScreen
            // 
            this.button_fullScreen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_fullScreen.Location = new System.Drawing.Point(1193, 5);
            this.button_fullScreen.Name = "button_fullScreen";
            this.button_fullScreen.Size = new System.Drawing.Size(116, 23);
            this.button_fullScreen.TabIndex = 4;
            this.button_fullScreen.Text = "Plein écran";
            this.button_fullScreen.UseVisualStyleBackColor = true;
            this.button_fullScreen.Click += new System.EventHandler(this.OnForceFullscreen);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(-5, 5);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1325, 700);
            this.tabControl1.TabIndex = 10;
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(1315, 701);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1128, 481);
            this.Name = "Form1";
            this.Text = "RPG Screen Caster [JB-DELUXE-EDITION]";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.splitContainer5.Panel1.ResumeLayout(false);
            this.splitContainer5.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer5)).EndInit();
            this.splitContainer5.ResumeLayout(false);
            this.splitContainer6.Panel1.ResumeLayout(false);
            this.splitContainer6.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer6)).EndInit();
            this.splitContainer6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.splitContainer_mainH.Panel1.ResumeLayout(false);
            this.splitContainer_mainH.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_mainH)).EndInit();
            this.splitContainer_mainH.ResumeLayout(false);
            this.splitContainer_leftV.Panel1.ResumeLayout(false);
            this.splitContainer_leftV.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_leftV)).EndInit();
            this.splitContainer_leftV.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel_searchContainer.ResumeLayout(false);
            this.tableLayoutPanel_searchContainer.PerformLayout();
            this.panel_switcher_search_tree.ResumeLayout(false);
            this.panel_switcher_search_tree.PerformLayout();
            this.panel_dirContent.ResumeLayout(false);
            this.panel_dirContent.PerformLayout();
            this.panel_loadbar.ResumeLayout(false);
            this.splitContainer_rightV.Panel1.ResumeLayout(false);
            this.splitContainer_rightV.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_rightV)).EndInit();
            this.splitContainer_rightV.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_preview)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar_width)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowpanel_pdfPages.ResumeLayout(false);
            this.flowpanel_pdfPages.PerformLayout();
            this.tableLayoutPanel_favContainer.ResumeLayout(false);
            this.panel_favBar.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer splitContainer4;
        private System.Windows.Forms.SplitContainer splitContainer5;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.SplitContainer splitContainer6;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.SplitContainer splitContainer_mainH;
        private System.Windows.Forms.SplitContainer splitContainer_leftV;
        private System.Windows.Forms.TreeView treeView_dirs;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_dirContent;
        private System.Windows.Forms.SplitContainer splitContainer_rightV;
        private System.Windows.Forms.Button button_point;
        private System.Windows.Forms.PictureBox pictureBox_preview;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_favsContent;
        private System.Windows.Forms.Button button_loadDir;
        private System.Windows.Forms.Label label_dirPath;
        private System.Windows.Forms.Button button_fullScreen;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_favContainer;
        private System.Windows.Forms.Panel panel_favBar;
        private System.Windows.Forms.Button button_loadFav;
        private System.Windows.Forms.Button button_saveFav;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_searchContainer;
        private System.Windows.Forms.Label label_search;
        private System.Windows.Forms.TextBox searchBox;
        private System.Windows.Forms.Panel panel_switcher_search_tree;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel_searchResult;
        private System.Windows.Forms.Button button_search;
        private System.Windows.Forms.Label searchResult;
        private System.Windows.Forms.Button button_clearSearch;
        private System.Windows.Forms.ProgressBar progressBar_search;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowpanel_pdfPages;
        private System.Windows.Forms.Button button_nextPage;
        private System.Windows.Forms.Label label_pdfPage;
        private System.Windows.Forms.Button button_prevPage;
        private System.Windows.Forms.Panel panel_dirContent;
        private System.Windows.Forms.ProgressBar progressBar_dirContent;
        private System.Windows.Forms.Panel panel_loadbar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button_eraser;
        private System.Windows.Forms.Button button_pen;
        private System.Windows.Forms.Button button_colorpicker;
        private System.Windows.Forms.TrackBar trackBar_width;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label_width;
        private System.Windows.Forms.Panel panel_selectedColor;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button_undoDraw;
        private System.Windows.Forms.Button button_redoDraw;
    }
}

